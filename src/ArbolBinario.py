class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None


class ArbolBinario:
    def __init__(self):
        self.raiz = None

    def insertar(self, elemento):
        if self.raiz is None:
            self.raiz = Nodo(elemento)
        else:
            self._insertar_recursivo(elemento, self.raiz)

    def _insertar_recursivo(self, elemento, nodo_actual):
        if elemento < nodo_actual.valor:
            if nodo_actual.izquierda is None:
                nodo_actual.izquierda = Nodo(elemento)
            else:
                self._insertar_recursivo(elemento, nodo_actual.izquierda)
        else:
            if nodo_actual.derecha is None:
                nodo_actual.derecha = Nodo(elemento)
            else:
                self._insertar_recursivo(elemento, nodo_actual.derecha)

    def eliminar(self, elemento):
        self.raiz = self._eliminar_recursivo(self.raiz, elemento)

    def _eliminar_recursivo(self, nodo_actual, elemento):
        if nodo_actual is None:
            return nodo_actual

        if elemento < nodo_actual.valor:
            nodo_actual.izquierda = self._eliminar_recursivo(nodo_actual.izquierda, elemento)
        elif elemento > nodo_actual.valor:
            nodo_actual.derecha = self._eliminar_recursivo(nodo_actual.derecha, elemento)
        else:
            if nodo_actual.izquierda is None:
                temp = nodo_actual.derecha
                nodo_actual = None
                return temp
            elif nodo_actual.derecha is None:
                temp = nodo_actual.izquierda
                nodo_actual = None
                return temp

            temp = self._minimo_valor_nodo(nodo_actual.derecha)
            nodo_actual.valor = temp.valor
            nodo_actual.derecha = self._eliminar_recursivo(nodo_actual.derecha, temp.valor)

        return nodo_actual

    def _minimo_valor_nodo(self, nodo):
        actual = nodo
        while actual.izquierda is not None:
            actual = actual.izquierda
        return actual

    def existe(self, elemento):
        return self._existe_recursivo(elemento, self.raiz) if self.raiz else False

    def _existe_recursivo(self, elemento, nodo_actual):
        if nodo_actual is None:
            return False
        if nodo_actual.valor == elemento:
            return True
        elif elemento < nodo_actual.valor:
            return self._existe_recursivo(elemento, nodo_actual.izquierda)
        else:
            return self._existe_recursivo(elemento, nodo_actual.derecha)

    def maximo(self):
        return self._maximo_recursivo(self.raiz) if self.raiz else None

    def _maximo_recursivo(self, nodo_actual):
        if nodo_actual.derecha is None:
            return nodo_actual.valor
        return self._maximo_recursivo(nodo_actual.derecha)

    def minimo(self):
        return self._minimo_recursivo(self.raiz) if self.raiz else None

    def _minimo_recursivo(self, nodo_actual):
        if nodo_actual.izquierda is None:
            return nodo_actual.valor
        return self._minimo_recursivo(nodo_actual.izquierda)

    def altura(self):
        return self._altura_recursiva(self.raiz) if self.raiz else 0

    def _altura_recursiva(self, nodo_actual):
        if nodo_actual is None:
            return 0
        else:
            altura_izquierda = self._altura_recursiva(nodo_actual.izquierda)
            altura_derecha = self._altura_recursiva(nodo_actual.derecha)
            return max(altura_izquierda, altura_derecha) + 1
    
    def obtener_elementos(self):
        elementos = []
        self._obtener_elementos_recursivo(self.raiz, elementos)
        return elementos

    def _obtener_elementos_recursivo(self, nodo_actual, elementos):
        if nodo_actual is not None:
            self._obtener_elementos_recursivo(nodo_actual.izquierda, elementos)
            elementos.append(nodo_actual.valor)
            self._obtener_elementos_recursivo(nodo_actual.derecha, elementos)

    def __str__(self):
       elementos = self.obtener_elementos()
       return "[ " + " ".join(map(str, elementos)) + " ]"

